package com.atlassian.bamboo.plugin.graphiteconnector;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.Socket;

/**
 * Configuration for GarphiteConnector
 */
public interface GraphiteConnectorConfiguration {

    Socket getSocket() throws IOException;

    /**
     * This method is existing only to work-around a frontend issue. DON'T call it.
     *  As this method is marked as do not use, I setting it as deprecated until fix can be used
     *
     * @param graphiteSendingDisabled
     */
    @Deprecated
    void setGraphiteSendingDisabled(Boolean graphiteSendingDisabled);

    /**
     * Returns if graphite connector is disabled.
     * @return True if the graphite connector is disabled.
     */
    Boolean getGraphiteSendingDisabled();

    /**
     * Enables or disables the GraphiteConnector
     *
     * If the connector is disabled, methods are still callable, but messages wont be
     * forwarded to the graphite server.
     *
     * @param graphiteSendingEnabled
     */
    void setGraphiteSendingEnabled(Boolean graphiteSendingEnabled);


    /**
     * Returns if the graphite connector is enabled.
     * @return True if the graphite connector is enabled.
     */
    Boolean getGraphiteSendingEnabled();

    /**
     * Sets the Graphite Server Host Name
     * @param hostName HostName of the Graphite Server without any prefix or suffix.
     */
    void setGraphiteHostName(@NotNull String hostName );

    /**
     * Returns the current Graphite Server Host Name
     * @return HostName of the Graphite Server
     */
    String getGraphiteHostName();

    /**
     * Sets the Graphite Server Port
     * @param port Port where the Graphite Server listens for text protocol input.
     */
    void setGraphitePort( Integer port );

    /**
     * Returns the current Graphite Server Port
     * @return Current Port of Graphite Server
     */
    Integer getGraphitePort();

    /**
     * Returns the pattern used to generate the Graphite Path
     * @return Pattern for the Graphite Path.
     */
    String getPathPattern();

    /**
     * Return the HostName that will be sent to Graphite.
     *
     * The HostName is defined by the BaseUrl of the Bamboo Server.
     *
     * @return
     */
    String getSourceHostname();

}
