package ut.com.atlassian.bamboo.plugin.graphiteconnector;

import com.atlassian.bamboo.plugin.graphiteconnector.GraphiteConnectorConfiguration;
import com.atlassian.bamboo.plugin.graphiteconnector.GraphiteConnectorConstants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


/**
 * Created with IntelliJ IDEA.
 * User: dkovacs
 * Date: 31/07/13
 * Time: 12:03 PM
 */

@RunWith(MockitoJUnitRunner.class)
public class GraphiteMessageFormattingTest {

    @Mock
    private GraphiteConnectorConfiguration configuration;

    @Before
    public void setup() throws IOException
    {
        when(configuration.getSourceHostname()).thenReturn("MyHost");
        when(configuration.getPathPattern()).thenReturn("%s.%s.%s");
    }

    @Test
    public void testIfSimpleMessageFormattedCorrectly() {
        final String testPath = "testPath1.";
        final String testLabel = "testLabel";
        final String testValue = "0";
        final int testTimestamp = 0;
        final String expectedMessage = "MyHost.testPath1.testLabel 0 0";
        final String message = GraphiteConnectorConstants.formatGraphiteMessage( configuration, testPath, testLabel, testValue, testTimestamp );
        assertEquals( "Message formatted incorrectly.", expectedMessage, message );
    }

    @Test
    public void testIfMissingLabelFormattedCorrectly()
    {
        final String testPath = "testPath1.testPath2.testPath3";
        final String testLabel = "";
        final String testValue = "0";
        final int testTimestamp = 0;
        final String expectedMessage = "MyHost.testPath1.testPath2.testPath3 0 0";
        final String message = GraphiteConnectorConstants.formatGraphiteMessage( configuration, testPath, testLabel, testValue, testTimestamp );
        assertEquals( "Message formatted incorrectly.", expectedMessage, message );
    }

    @Test
    public void testIfMissingPathFormattedCorrectly()
    {
        final String testPath = "";
        final String testLabel = "label1";
        final String testValue = "0";
        final int testTimestamp = 0;
        final String expectedMessage = "MyHost.label1 0 0";
        final String message = GraphiteConnectorConstants.formatGraphiteMessage( configuration, testPath, testLabel, testValue, testTimestamp );
        assertEquals( "Message formatted incorrectly.", expectedMessage, message );
    }

    @Test
    public void testIfDoubleDotsHandledCorrectly()
    {
        final String testPath = "testPath1.testPath2.";
        final String testLabel = "testLabel1";
        final String testValue = "0";
        final int testTimestamp = 0;
        final String expectedMessage = "MyHost.testPath1.testPath2.testLabel1 0 0";
        final String message = GraphiteConnectorConstants.formatGraphiteMessage( configuration, testPath, testLabel, testValue, testTimestamp );
        assertEquals( "Message formatted incorrectly.", expectedMessage, message );
    }

    @Test
    public void testIfMultiLevelPathsHandledCorrectly()
    {
        final String testPath = "testPath1.testPath2";
        final String testLabel = ".testPath3.testLabel1";
        final String testValue = "0";
        final int testTimestamp = 0;
        final String expectedMessage = "MyHost.testPath1.testPath2.testPath3.testLabel1 0 0";
        final String message = GraphiteConnectorConstants.formatGraphiteMessage( configuration, testPath, testLabel, testValue, testTimestamp );
        assertEquals( "Message formatted incorrectly.", expectedMessage, message );
    }

    @Test
    public void testIfDotOnTheEndHandledCorrecly()
    {
        final String testPath = "testPath1.testPath2";
        final String testLabel = "testLabel1.";
        final String testValue = "0";
        final int testTimestamp = 0;
        final String expectedMessage = "MyHost.testPath1.testPath2.testLabel1 0 0";
        final String message = GraphiteConnectorConstants.formatGraphiteMessage( configuration, testPath, testLabel, testValue, testTimestamp );
        assertEquals( "Message formatted incorrectly.", expectedMessage, message );
    }


}
